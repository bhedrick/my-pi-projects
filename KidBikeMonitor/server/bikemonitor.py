import RPi.GPIO as GPIO
import time

PIR = 23
LED = 24

pirState = False                        # we start, assuming no motion detected
pirVal = False                          # we start, assuming no motion detected

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR, GPIO.IN)
GPIO.setup(LED, GPIO.OUT)

try:
    while True:
        pirVal = GPIO.input(PIR)            # read input value
        if (pirVal == True):                # check if the input is HIGH
            GPIO.output(LED, True)          # turn LED ON
            if (pirState == False):
                # we have _just_ turned on
                pirState = True
        else:
            GPIO.output(LED, False)         # turn LED OFF
            if (pirState == True):
                # we have _just_ turned off
                time.sleep(2)
                pirState = False;
except (KeyboardInterrupt, SystemExit):
    GPIO.cleanup()
    