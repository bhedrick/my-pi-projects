import redis
import sys

r = redis.StrictRedis(host='localhost', port=6379, db=0, socket_connect_timeout=5, socket_timeout=5)
print r.set('brooke_test:foo', 'bar') # return True
print r.get('brooke_test:foo') # return 'bar'
print r.get('brooke_test:missing') # return None

r = redis.StrictRedis(host='invalidhostname', port=6379, db=0, socket_connect_timeout=5, socket_timeout=5)
print "connected 2"
try:
    print r.get('brooke_test:foo') # redis.exceptions.ConnectionError, invalid port is the same error
except: # catch *all* exceptions
    e = sys.exc_info()[0]
    print( "Error: %s" % e )

