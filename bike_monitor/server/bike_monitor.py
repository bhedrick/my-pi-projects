#!/usr/bin/env python
#
# main.py for Ship-It - must be run as root
#

from datetime import datetime
import json
import os
import signal
from time import sleep
import RPi.GPIO as GPIO
import sys
import yaml
from ftplib import FTP
from ftplib import FTP_TLS


settings = {}

#GPIO.setmode(GPIO.BOARD)
GPIO.setmode(GPIO.BCM)

# Push Notification setup
from pushetta import Pushetta

# Define which 'physical' GPIO pins are to be used for the buttons
buttonC=18
buttonB=17


def initPins():
  # Set up the GPIO pins to be pull-down so that they are triggered when 
  # current from the corresponding RF receiver goes through
  GPIO.setup(buttonB,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
  GPIO.setup(buttonC,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
  # Add event detection to GPIO pins
  GPIO.add_event_detect(buttonB, GPIO.RISING, pushMsg, bouncetime=50)
  GPIO.add_event_detect(buttonC, GPIO.RISING, pushMsg, bouncetime=50)


def cleanupPins():
    "cleanup everything we created with the GPIO"
    print("[" + getNow() + "] cleaning up pins!")
    GPIO.cleanup()
    
    
def getNow():
    "get the current date and time as a string"
    return datetime.now().strftime('%m/%d/%Y %H:%M:%S')
    
    
def pushMsg(channel):
  msg_time = getNow()
  button = getButton(channel)
  owner = getOwner(channel)
  
  # send message to pushetta
  msg = "%s's bicycle %s seen at %s" % (owner, button, msg_time)
  p=Pushetta(settings['pushetta']['api_key'])
  p.pushMessage(settings['pushetta']['channel_name'], msg)
  print "%s %s" % (msg_time, msg)

  # write json data file to web server folder
  data = {}
  if os.path.isfile('/var/www/html/bike_monitor/index.html'):
    with open('/var/www/html/bike_monitor/index.html') as json_data:
      data = json.load(json_data)
  else:
      # initiatlze the data
      data['master'] = {}
      data['master']['last_updated'] = ''
      data['B'] = {}
      data['B']['owner'] = getOwner(buttonB)
      data['B']['last_seen'] = ''
      data['C'] = {}
      data['C']['owner'] = getOwner(buttonC)
      data['C']['last_seen'] = ''

  data['master']['last_updated'] = msg_time
  data[button]['owner'] = owner
  data[button]['last_seen'] = msg_time
  with open('/var/www/html/bike_monitor/index.html', 'w') as outfile:
    json.dump(data, outfile)

  ftp = FTP(settings['ftp']['host'], settings['ftp']['username'], settings['ftp']['password'])     # connect to host, default port
  ftp.cwd(settings['ftp']['remote_path'])
  with open('/var/www/html/bike_monitor/index.html') as json_data:
    ftp.storbinary("STOR %s" % (settings['ftp']['remote_file']), json_data) 
  ftp.quit()

def getButton(channel):
   return{
      buttonB: 'B',
      buttonC: 'C',
   }[channel]


def getOwner(channel):
   return{
      buttonB: 'Drew',
      buttonC: 'Kate',
   }[channel]


def sigterm_handler(_signo, _stack_frame):
    "When sysvinit sends the TERM signal, cleanup before exiting."
    print("[" + getNow() + "] received signal {}, exiting...".format(_signo))
    cleanupPins()
    sys.exit(0)


signal.signal(signal.SIGTERM, sigterm_handler)    


if __name__ == "__main__":
  """init some pins and run the loop"""
  with open("/etc/bike_monitor.yaml", 'r') as stream:
    settings = yaml.load(stream)

  initPins()
  try:
    while True:
      sleep(4)
      #None
  except KeyboardInterrupt:
    cleanupPins()

