import RPi.GPIO as GPIO
import time

LED = 24

GPIO.setmode(GPIO.BCM)
GPIO.setup(LED, GPIO.OUT)

try:
    print "Hi"
    while True:
        print "on"
        GPIO.output(LED, True)
        time.sleep(1)
        print "off"
        GPIO.output(LED, False)
        time.sleep(1)
except (KeyboardInterrupt, SystemExit):
    GPIO.cleanup()
    